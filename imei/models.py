# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from __future__ import unicode_literals
from django.db.models.signals import post_save, post_delete
from django.db import models


groupOpt = (('ESP','ESP'),('CII','CII'))

class ImeiExcep(models.Model):
    imei = models.CharField(max_length=15, unique=True)
    grupo = models.CharField(max_length=3, choices=groupOpt)
    fecha = models.DateTimeField(auto_now_add=True)
    modificacion = models.DateTimeField(auto_now=True)
    add_by = models.CharField(max_length=6,default='normal', editable=False)

    def __str__(self):
        return self.imei


def ImeiExcepPostDelete(sender,instance, *args,**kwargs):
    print(instance)
    if(instance.add_by=='normal'):
        imei = ImeiExcep.objects.using('velez').get(id=instance.id)
        imei.delete(using='velez')
        print("delete")

post_delete.connect(ImeiExcepPostDelete,sender=ImeiExcep)

def ImeiExcepPostSave(sender,instance,*args,**kwargs):
    if(instance.add_by=='normal'):
        instance.add_by='signal'
        instance.save(using='velez')
        

post_save.connect(ImeiExcepPostSave,sender=ImeiExcep)


class VialeRouter:
    def db_for_read(self, model, **hints):
        if model._meta.app_label == 'imei':
            return 'viale'
        return None
    
    def db_for_write(self, model, **hints):
        if model._meta.app_label == 'imei':
            return 'viale'
        return None

    def allow_relation(self, obj1, obj2, **hints):
        if obj1._meta.app_label == 'imei' or \
           obj2._meta.app_label == 'imei':
           return True
        return None

    def allow_migrate(self, db, app_label, model_name=None, **hints):
        if app_label == 'imei':
            return db == 'viale'
        return None
