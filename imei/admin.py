# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render,redirect
from django.conf.urls import url
from django import forms
from django.contrib import admin
from . import models
from django.db import IntegrityError
import csv
from django.http import HttpResponse

class ImeiForm(forms.ModelForm):
    imei = forms.CharField(max_length=14, min_length=14)
    class meta:
        model = models.ImeiExcep

class CsvImportForm(forms.Form):
    csv_file = forms.FileField()

def export_as_csv(self, request, queryset):
    meta = self.model._meta
    field_names = [field.name for field in meta.fields]

    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename={}.csv'.format(meta)
    writer = csv.writer(response)

    writer.writerow(field_names)
    for obj in queryset:
        row = writer.writerow([getattr(obj, field) for field in field_names])

    return response

export_as_csv.short_description = "Export Selected"

class ImeiAdmin(admin.ModelAdmin):
    form = ImeiForm
    list_display = ('imei', 'grupo', 'fecha')
    search_fields = ['imei','grupo','fecha']
    list_filter = ['grupo']
    actions = [export_as_csv]

    change_list_template = "imei/changelist.html"

    def get_urls(self):
        urls = super(ImeiAdmin, self).get_urls()
        my_urls = [
            url(
                r'import_csv/',
                self.admin_site.admin_view(self.import_csv),
                name='import_csv',
            ),
        ]
        return my_urls + urls

    def import_csv(self, request):
        if request.method == "POST":
            csv_file = request.FILES["csv_file"]
            reader = csv.reader(csv_file)
            for row in reader:
                try:
                    imei_obj= models.ImeiExcep.objects.create(imei=row[0],grupo=row[1])
                except IntegrityError:
                    pass
            self.message_user(request, "Your csv file has been imported")
            return redirect("..")
        form = CsvImportForm()
        payload = {"form": form}
        return render(
            request, "imei/csv_form.html", payload
        )

admin.site.register(models.ImeiExcep, ImeiAdmin)
