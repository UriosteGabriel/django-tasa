# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from . import models 

# Register your models here.
class SmsAdmin(admin.ModelAdmin):
    list_display = ('key', 'value', 'origen', 'plan')
    search_fields = ['key', 'value', 'origen', 'plan']
    readonly_fields = ["key"]

    def get_readonly_fields(self, request, obj=None):
        if obj:
            return ["key"]
        else:
            return []

admin.site.register(models.GvpSms, SmsAdmin)
