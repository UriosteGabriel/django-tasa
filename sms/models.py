# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from __future__ import unicode_literals
from django.db.models.signals import post_save, post_delete
from django.db import models


groupPlan = (('P','P'),('C','C'),('F','F'))
class GvpSms(models.Model):
    key = models.CharField(primary_key=True,max_length=45,help_text="ID del mensaje de texto, utilizado por Genesys Rules.")
    value = models.CharField(max_length=245,blank=False,null=False,editable=True,help_text="Mensaje de texto que se va a enviar al cliente.")
    add_by = models.CharField(max_length=6,default='normal',editable=False)
    origen = models.CharField(max_length=10,blank=False,null=False,editable=True,help_text="Numero de origen del mensaje de texto.")
    plan = models.CharField(max_length=2,choices=groupPlan,help_text="Tipo de cliente: Prepago, Control y Full.")

    def __str__(self):
        return self.key

    class Meta:
        managed = False
        db_table = 'GVP_SMS'

def ImeiExcepPostDelete(sender,instance, *args,**kwargs):
    print(instance)
    if(instance.add_by=='normal'):
        imei = GvpSms.objects.using('velez').get(key=instance.key)
        imei.delete(using='velez')
        print("delete")

post_delete.connect(ImeiExcepPostDelete,sender=GvpSms)

def ImeiExcepPostSave(sender,instance,*args,**kwargs):
    if(instance.add_by=='normal'):
        instance.add_by='signal'
        instance.save(using='velez')
        

post_save.connect(ImeiExcepPostSave,sender=GvpSms)


class VialeRouter:
    def db_for_read(self, model, **hints):
        if model._meta.app_label == 'sms':
            return 'viale'
        return None
    
    def db_for_write(self, model, **hints):
        if model._meta.app_label == 'sms':
            return 'viale'
        return None

    def allow_relation(self, obj1, obj2, **hints):
        if obj1._meta.app_label == 'sms' or \
           obj2._meta.app_label == 'sms':
           return True
        return None

    def allow_migrate(self, db, app_label, model_name=None, **hints):
        if app_label == 'sms':
            return db == 'viale'
        return None
