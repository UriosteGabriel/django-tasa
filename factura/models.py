# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from __future__ import unicode_literals
from django.db.models.signals import post_save, post_delete
from django.db import models


class FacLiterales(models.Model):
    flid = models.CharField(primary_key=True, max_length=7, help_text="ID detalle de factura.")
    literal = models.CharField(max_length=150, help_text="Literal del detalle de factura.")
    audio = models.CharField(max_length=10, help_text="ID del audio en el GAX.")
    add_by = models.CharField(max_length=6, default='normal', editable=False)


    def __str__(self):
        return self.flid

    class Meta:
        managed = False
        db_table = 'FAC_LITERALES'

def ImeiExcepPostDelete(sender,instance, *args,**kwargs):
    print(instance)
    if(instance.add_by=='normal'):
        imei = FacLiterales.objects.using('velez').get(flid=instance.flid)
        imei.delete(using='velez')
        print("delete")

post_delete.connect(ImeiExcepPostDelete,sender=FacLiterales)

def ImeiExcepPostSave(sender,instance,*args,**kwargs):
    if(instance.add_by=='normal'):
        instance.add_by='signal'
        instance.save(using='velez')
        

post_save.connect(ImeiExcepPostSave,sender=FacLiterales)


class VialeRouter:
    def db_for_read(self, model, **hints):
        if model._meta.app_label == 'factura':
            return 'viale'
        return None
    
    def db_for_write(self, model, **hints):
        if model._meta.app_label == 'factura':
            return 'viale'
        return None

    def allow_relation(self, obj1, obj2, **hints):
        if obj1._meta.app_label == 'factura' or \
           obj2._meta.app_label == 'factura':
           return True
        return None

    def allow_migrate(self, db, app_label, model_name=None, **hints):
        if app_label == 'factura':
            return db == 'viale'
        return None

