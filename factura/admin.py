# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from . import models

# Register your models here.
class FacLiteralesAdmin(admin.ModelAdmin):
    list_display = ('flid', 'literal', 'audio')
    search_fields = ['literal', 'audio']
    readonly_fields = ["flid"]

    def get_readonly_fields(self, request, obj=None):
        if obj:
            return ["flid"]
        else:
            return []

admin.site.register(models.FacLiterales, FacLiteralesAdmin)

