# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib import admin
from . import models

# Register your models here.
class CmpSmsAdmin(admin.ModelAdmin):
    list_display = ('cmp_id', 'cmp_header', 'ba_servicio', 'ba_estado', 'ba_tarea', 'cmp_status')
    search_fields = ['cmp_id', 'ba_servicio', 'ba_estado', 'ba_tarea']
    readonly_fields = ["cmp_id"]

    def get_readonly_fields(self, request, obj=None):
        if obj:
            return ["cmp_id"]
        else:
            return []

admin.site.register(models.DialerSms, CmpSmsAdmin)
