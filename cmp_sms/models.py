# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from __future__ import unicode_literals
from django.db.models.signals import post_save, post_delete
from django.db import models

groupStatus = (('disable','Deshabilitada'),('enable','Habilitada'))
groupService = (('ProximoVencimiento','ProximoVencimiento'),('Marketing','Marketing'),('Varios','Varios'))
class DialerSms(models.Model):
    cmp_id = models.CharField(primary_key=True, max_length=20, help_text="ID de la Campana.")
    cmp_header = models.CharField(max_length=250, blank=False, null=False, help_text="Definicion de Headers de la Campana.")
    ba_servicio = models.CharField(max_length=35, choices=groupService, null=False, help_text="Definicion del Business Attribute para Servicio.")
    ba_estado = models.CharField(max_length=35, blank=False, null=False, help_text="Definicion del Business Attribute para Estados.")
    ba_tarea = models.CharField(max_length=35, blank=False, null=False, help_text="Definicion del Business Attribute para Tareas.")
    cmp_status = models.CharField(max_length=20, choices=groupStatus, help_text="Estado de la Campana.")
    add_by = models.CharField(max_length=6, default='normal', editable=False)

    def __str__(self):
        return self.cmp_id

    class Meta:
        managed = False
        db_table = 'DIALER_SMS'

def ImeiExcepPostDelete(sender,instance, *args,**kwargs):
    print(instance)
    if(instance.add_by=='normal'):
        imei = DialerSms.objects.using('velez').get(cmp_id=instance.cmp_id)
        imei.delete(using='velez')
        print("delete")

post_delete.connect(ImeiExcepPostDelete,sender=DialerSms)

def ImeiExcepPostSave(sender,instance,*args,**kwargs):
    if(instance.add_by=='normal'):
        instance.add_by='signal'
        instance.save(using='velez')
        

post_save.connect(ImeiExcepPostSave,sender=DialerSms)


class VialeRouter:
    def db_for_read(self, model, **hints):
        if model._meta.app_label == 'cmp_sms':
            return 'viale'
        return None
    
    def db_for_write(self, model, **hints):
        if model._meta.app_label == 'cmp_sms':
            return 'viale'
        return None

    def allow_relation(self, obj1, obj2, **hints):
        if obj1._meta.app_label == 'cmp_sms' or \
           obj2._meta.app_label == 'cmp_sms':
           return True
        return None

    def allow_migrate(self, db, app_label, model_name=None, **hints):
        if app_label == 'cmp_sms':
            return db == 'viale'
        return None
